import com.amazonaws.services.cloudfront.CloudFrontCookieSigner;
import com.amazonaws.services.cloudfront.CloudFrontUrlSigner;
import com.amazonaws.services.cloudfront.util.SignerUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hoangnm on 11/7/18.
 */
public class CookieSigner {

    public static void main(String[] args) throws ParseException {
        // Remember:
        // 1. Set Restrict Viewer Access to YES in CloudFront Distribution > Behavior
        // 2. Configure to only access file via CloudFront use Origin Access Identity
        // https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-task-list.html

//        String distributionDomain = "d1co1wp5pf67bf.cloudfront.net";
//        String distributionid = "E1QV8FM7AT7H01"; //Cloud Front Distribution id
//        String KeyFileId = "APKAJRHLMCW6PQAK3MSA"; //AWS PEM KEY FILE ID, in My Account/My Security Credentials
        // Must config OAI to get via REST
        // https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-restricting-access-to-s3.html


        String distributionDomain = "d2q4jo8nys80xc.cloudfront.net";
        String distributionid = "E1QV8FM7AT7H01"; //Cloud Front Distribution id
        String KeyFileId = "APKAIF7YGYFJQ3XOIVSA"; //AWS PEM KEY FILE ID, in My Account/My Security Credentials
//        String KeyFileId = "APKAIYRQHIZVYQISYJGA"; //AWS PEM KEY FILE ID, in My Account/My Security Credentials

//        String s3ObjectKey = "1541135322172_aaa.png";
        String s3ObjectKey = "6163eb37-3c44-4ee2-aa34-6f73ea7a5f39_1542379776230_test.png";

        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        String string1 = "2018-11-20T12:08:56.235-0700";
        Date expiresOn = df1.parse(string1);
//        Date expiresOn = (new LocalDate(2018, 11, 10)).plusYears(1).toDate();
//        ServiceUtils.parseIso8601Date(string1);




        String policyResourcePath = "https://" + distributionDomain + "/" + s3ObjectKey;

        ClassLoader classLoader = new CookieSigner().getClass().getClassLoader();
        File privateKeyFile = new File(classLoader.getResource("cookie-prod.der").getFile());

        CloudFrontCookieSigner.CookiesForCannedPolicy cookies = null;

        try {
//            PrivateKey privateKey = convertDerToPrivateKey(privateKeyFile);
            PrivateKey privateKey = SignerUtils.loadPrivateKey(privateKeyFile);
//            cookies = CloudFrontCookieSigner.getCookiesForCannedPolicy(policyResourcePath, KeyFileId, privateKeyFile, expiresOn);
            cookies = CloudFrontCookieSigner.getCookiesForCannedPolicy(policyResourcePath, KeyFileId, privateKey, expiresOn);

            String url1 = CloudFrontUrlSigner.getSignedURLWithCannedPolicy(policyResourcePath, KeyFileId, privateKey, expiresOn);
            System.out.println("URL: " + url1);

            //  @SuppressWarnings({ "resource", "deprecation" })
            HttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(SignerUtils.generateResourcePath(SignerUtils.Protocol.https, distributionDomain,
                    s3ObjectKey));

            httpGet.addHeader("Cookie", cookies.getExpires().getKey() + "=" +cookies.getExpires().getValue());
            httpGet.addHeader("Cookie", cookies.getSignature().getKey() + "=" + cookies.getSignature().getValue());
            httpGet.addHeader("Cookie", cookies.getKeyPairId().getKey() + "=" + cookies.getKeyPairId().getValue());
            HttpResponse responsevalues = client.execute(httpGet);
            System.out.println(responsevalues);

        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static PrivateKey convertDerToPrivateKey(File derFile) throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        // Read file to a byte array.
        byte[] privKeyByteArray = Files.readAllBytes(derFile.toPath());
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privKeyByteArray);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey myPrivKey = keyFactory.generatePrivate(keySpec);
        System.out.println("Algorithm: " + myPrivKey.getAlgorithm());
        return myPrivKey;
    }
}
